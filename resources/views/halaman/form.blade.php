@extends('layout.master')

@section('judul')
Signup Form
@endsection


@section('content')
    <form action="/welcome" method="post">
    @csrf
    <label>First name:</label><br>
    <input type="text" name="fname" placeholder="Enter your first name" required><br><br>
    <label>Last name:</label><br>
    <input type="text" name="lname" placeholder="Enter your last name" required><br><br>
    
    <label>Gender:</label><br>
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="Other">
        <label for="other">Other</label><br><br>
    
    <label>Nationality:</label><br>
    <select name="nationality">
        <option value="1">WNA</option>
        <option value="1">WNI</option>
    </select> <br><br>

    <label>Language Spoken:</label><br>
    <input type="checkbox" name="languageSkill">Bahasa Indonesia <br>
    <input type="checkbox" name="languageSkill">English <br>
    <input type="checkbox" name="languageSkill">Other <input type="text" id="languageSkill" placeholder="Enter your other languages"> <br> <br>
    
    <label>Bio:</label> <br>
    <textarea name="bio" cols="30" rows="10"></textarea> <br><br>

    <input type="submit" value="Submit">
    </form>
@endsection