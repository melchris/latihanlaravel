<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/index', function () {
//     return view('halaman.index');
// });

Route::get('/', 'DashboardController@index');

Route::get('/form', 'FormController@bio');

Route::post('/welcome', 'FormController@Submit');

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/data-table', function(){
    return view('tables.data-table');
});

Route::get('/table', function(){
    return view('tables.table');
});

// CRUD Cast
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('cast', 'CastController');
});


Auth::routes();

